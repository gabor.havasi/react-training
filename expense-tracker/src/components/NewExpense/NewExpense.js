import { useState } from "react";
import ExpenseForm from "./ExpenseForm";
import "./NewExpense.css";

const NewExpense = ({ onAddExpense }) => {
  const [isFormEdit, setIsFormEdit] = useState(false);

  const saveExpenseDataHandler = (data) => {
    data.id = Date.now();
    data.date = new Date(data.date);
    onAddExpense(data);
    setIsFormEdit(false);
  };

  const addNewExpenseHandler = () => {
    setIsFormEdit(true);
  };

  const cancelHandler = () => {
    setIsFormEdit(false);
  };

  return (
    <div className="new-expense">
      {!isFormEdit && (
        <button onClick={addNewExpenseHandler}>Add a new expense</button>
      )}
      {isFormEdit && (
        <ExpenseForm
          onSubmit={saveExpenseDataHandler}
          onClick={cancelHandler}
        />
      )}
    </div>
  );
};

export default NewExpense;
