import "./ChartBar.css";

const ChartBar = ({ value, maxValue, label }) => {
  let barHight = "0%";

  if (maxValue > 0) {
    barHight = Math.round((value / maxValue) * 100) + "%";
    console.log(barHight);
  }
  console.log({ value, maxValue, label });

  return (
    <div className="chart-bar">
      <div className="chart-bar__inner">
        <div className="chart-bar__fill" style={{ height: barHight }}></div>
      </div>
      <div className="chart-bar__label">{label}</div>
    </div>
  );
};

export default ChartBar;
