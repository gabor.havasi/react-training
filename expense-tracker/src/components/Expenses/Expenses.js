import { useState } from "react";
import Card from "../../Card/Card";
import ExpensesChart from "../ExpensesChart/ExpensesChart";
import ExpensesFilter from "../ExpensesFilter/ExpensesFilter";
import ExpensesList from "../ExpensesList/ExpensesList";
import "./Expenses.css";

const Expenses = ({ props }) => {
  const [filter, setFilter] = useState("2023");

  const onChange = (event) => {
    setFilter(event.target.value);
  };

  const filteredExpensesArray = props.filter(
    (e) => e.date.getFullYear().toString() === filter
  );

  return (
    <Card className="expenses">
      <h2>Let's get started!</h2>
      <ExpensesChart expenses={filteredExpensesArray} />
      <ExpensesFilter selected={filter} onChange={onChange} />
      <ExpensesList
        expenses={filteredExpensesArray}
        selected={filter}
        onChange={onChange}
      />
    </Card>
  );
};

export default Expenses;
