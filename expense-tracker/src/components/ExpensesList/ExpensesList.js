import ExpenseItem from "../ExpenseItem/ExpenseItem";

const ExpensesList = ({ expenses }) => {
  if (expenses.length === 0) {
    return <h2 className="='expenses-list__fallback">No expense was found.</h2>;
  }
  return (
    <ul className="expenses-list">
      {expenses.map((p) => (
        <ExpenseItem
          key={p.id}
          title={p.title}
          amount={p.amount}
          date={p.date}
        />
      ))}
    </ul>
  );
};

export default ExpensesList;
