import Expenses from "./components/Expenses/Expenses";
import NewExpense from "./components/NewExpense/NewExpense";
import { expenses } from "./components/mockData";
import { useState } from "react";

const App = () => {
  const [expensesData, setExpensesData] = useState(expenses);

  const saveExpenseDataHandler = (data) => {
    setExpensesData((prevState) => [...prevState, data]);
  };

  return (
    <div>
      <NewExpense onAddExpense={saveExpenseDataHandler} />
      <Expenses props={expensesData} />
    </div>
  );
};

export default App;
