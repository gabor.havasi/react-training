import Link from "next/link";
import classes from "./meals.module.css";
import Image from "next/image";

const MealItem = (props) => {
  const { title, slug, image, summary, creator } = props;
  return (
    <article className={classes.meal}>
      <header>
        <div className={classes.image}>
          <Image src={image} alt={title} fill />
        </div>
        <div className={classes.headerText}>
          <h4>{title}</h4>
          <p>by {creator}</p>
        </div>
      </header>
      <div className={classes.content}>
        <p className={classes.summary}>{summary}</p>
        <div className={classes.actions}>
          <Link href={`/meals/${slug}`}>View Recipe</Link>
        </div>
      </div>
    </article>
  );
};

export default MealItem;
