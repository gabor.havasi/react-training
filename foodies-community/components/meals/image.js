"use client";

import { useRef, useState } from "react";
import classes from "./meals.module.css";
import Image from "next/image";

const ImagePicker = ({ label, name }) => {
  const imageInput = useRef();
  const [selectedImage, setSelectedImage] = useState(null);

  const handleClick = () => {
    imageInput.current.click();
  };

  const handleChange = (event) => {
    const file = event.target.files[0];
    if (!file) {
      setSelectedImage(null);
      return;
    }

    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);
    setSelectedImage(URL.createObjectURL(file));
  };

  return (
    <div className={classes.picker}>
      <label htmlFor={name}>Image URL</label>
      <div className={classes.controls}>
        <div className={classes.preview}>
          {selectedImage && <Image src={selectedImage} alt="Preview" fill />}
        </div>
        <input
          className={classes.input}
          type="file"
          id={name}
          name={name}
          accept="image/png, image/jpeg"
          required
          ref={imageInput}
          onChange={handleChange}
        />
        <button className={classes.button} type="button" onClick={handleClick}>
          Pick image
        </button>
      </div>
    </div>
  );
};

export default ImagePicker;
