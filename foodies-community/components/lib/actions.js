"use server";

import { redirect } from "next/navigation";
import { addMeal } from "./meals";
import { revalidatePath } from "next/cache";

export const shareMeal = async (prevState, formData) => {
  const isValid =
    formData.get("name") &&
    formData.get("email") &&
    formData.get("title") &&
    formData.get("summary") &&
    formData.get("instructions") &&
    formData.get("image");

  const meal = {
    title: formData.get("title"),
    summary: formData.get("summary"),
    instructions: formData.get("instructions"),
    image: formData.get("image"),
    creator: formData.get("name"),
    creator_email: formData.get("email"),
  };

  if (!isValid) return { message: "Please fill in all fields" };

  await addMeal(meal);
  revalidatePath("/meals");
  redirect("/meals");
};
