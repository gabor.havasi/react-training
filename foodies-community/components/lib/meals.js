import sql from "better-sqlite3";
import slugify from "slugify";
// import xxs from "xxs";
import fs from "fs";

const db = sql("meals.db");

export const getMeals = async () => {
  await new Promise((resolve) => setTimeout(resolve, 1000));
  const meals = db.prepare("SELECT * FROM meals").all();

  return meals;
};

export const getMeal = (slug) => {
  const meal = db.prepare("SELECT * FROM meals WHERE slug = ?").get(slug);

  return meal;
};

export const addMeal = async (meal) => {
  meal.slug = slugify(meal.title, { lower: true });
  // meal.instructions = xxs(meal.instructions);

  const extension = meal.image.name.split(".").pop();
  const filename = `${meal.slug}.${Math.random() * 100}.${extension}`;
  const bufferImage = await meal.image.arrayBuffer();
  fs.createWriteStream(`public/images/${filename}`).write(
    Buffer.from(bufferImage),
    (err) => {
      if (err) {
        throw new Error(err);
      }
    }
  );

  meal.image = `/images/${filename}`;

  const insert = db.prepare(
    "INSERT INTO meals (title, slug, summary, instructions, image, creator, creator_email) VALUES (@title, @slug, @summary, @instructions, @image, @creator, @creator_email)"
  );

  const result = insert.run(meal);

  return result.changes === 1;
};
