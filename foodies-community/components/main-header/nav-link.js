"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";
import classes from "./header.module.css";

const NavLink = ({ target, children }) => {
  const currentPath = usePathname();
  return (
    <li>
      <Link
        href={target}
        className={currentPath.startsWith(target) ? classes.active : undefined}
      >
        {children}
      </Link>
    </li>
  );
};

export default NavLink;
