import Link from "next/link";
import Image from "next/image";
import logo from "../../assets/logo.png";
import classes from "./header.module.css";
import NavLink from "./nav-link";

const MainHeader = () => {
  return (
    <header className={classes.header}>
      <Link href="/" className={classes.logo}>
        <Image src={logo} alt="NextLevel Food" />
        Foodies Community
      </Link>
      <nav className={classes.nav}>
        <ul>
          <NavLink target="/meals">View Meals</NavLink>
          <NavLink target="/community">Community</NavLink>
        </ul>
      </nav>
    </header>
  );
};

export default MainHeader;
