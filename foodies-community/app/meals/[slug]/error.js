"use client";

const NotFound = () => {
  return (
    <main className="not-found">
      <h1> Meal Not Found </h1>{" "}
      <p> Oops! The meal you are looking for has been removed or relocated. </p>{" "}
    </main>
  );
};

export default NotFound;
