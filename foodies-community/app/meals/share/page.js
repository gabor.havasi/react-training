"use client";
import { useFormState } from "react-dom";
import ImagePicker from "@/components/meals/image";

import classes from "./page.module.css";
import { shareMeal } from "@/components/lib/actions";
import SubmitButton from "./submit-button";

const SharePage = () => {
  const [state, formAction] = useFormState(shareMeal, { message: null });
  return (
    <>
      <header className={classes.header}>
        <h1>
          Share your <span className={classes.highlight}>favourite recipe</span>
        </h1>
        <p>
          Fill in the form below to share your favourite recipe with the world.
        </p>
      </header>

      <main className={classes.main}>
        <form className={classes.form} action={formAction}>
          <div className={classes.row}>
            <p>
              <label htmlFor="title">Your name</label>
              <input type="text" id="name" name="name" required />
            </p>
            <p>
              <label htmlFor="email">Your email</label>
              <input type="email" id="email" name="email" required />
            </p>
          </div>
          <p>
            <label htmlFor="title">Recipe title</label>
            <input type="text" id="title" name="title" required />
          </p>
          <p>
            <label htmlFor="summary">Recipe summary</label>
            <textarea id="summary" name="summary" required></textarea>
          </p>
          <p>
            <label htmlFor="instructions">Instructions</label>
            <textarea
              id="instructions"
              name="instructions"
              rows={10}
              required
            ></textarea>
          </p>
          <div>
            <ImagePicker label="image" name="image" />
          </div>
          <p>{state.message && <span>{state.message}</span>}</p>
          <p className={classes.actions}>
            <SubmitButton />
          </p>
        </form>
      </main>
    </>
  );
};

export default SharePage;
