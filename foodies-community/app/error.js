"use client";

const Error = () => {
  return (
    <main className="error">
      <h1>500 - Server error</h1>
      <p>Oops! There is some error. Try again later.</p>
    </main>
  );
};

export default Error;
