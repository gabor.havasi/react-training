import Image from "next/image";

import mealIcon from "@/assets/icons/meal.png";
import communityIcon from "@/assets/icons/community.png";
import eventsIcon from "@/assets/icons/events.png";
import classes from "./page.module.css";

const CommunityPage = () => {
  return (
    <>
      <header className={classes.header}>
        <h1>
          One shared passion: <span className={classes.highlight}>Food</span>
        </h1>
        <p>Join our community and share your favourite recipes!</p>
      </header>
      <main className={classes.main}>
        <section className={classes.perks}>
          <h2>Discover new recipes</h2>
          <p>
            Explore a wide range of recipes shared by our community members.
          </p>
          <Image src={mealIcon} alt="A plate with a knife and fork" />
        </section>
        <section className={classes.perks}>
          <h2>Connect with other foodies</h2>
          <p>Share your recipes and connect with other food lovers.</p>
          <Image src={communityIcon} alt="A group of people" />
        </section>
        <section className={classes.perks}>
          <h2>Join our events</h2>
          <p>Participate in our food events and meet other foodies.</p>
          <Image src={eventsIcon} alt="A calendar" />
        </section>
      </main>
    </>
  );
};

export default CommunityPage;
