const NotFound = () => {
  return (
    <main className="not-found">
      <h1> 404 - Page Not Found </h1>{" "}
      <p> Oops! The page you are looking for has been removed or relocated. </p>{" "}
    </main>
  );
};

export default NotFound;
