import { useState } from "react";
import AddUser from "./components/Users/AddUser";
import UsersList from "./components/Users/UsersList";

function App() {
  const [userList, setUserList] = useState([]);

  const handleAddUser = (userName, age) => {
    setUserList((prevState) => {
      return [
        ...prevState,
        { name: userName, age: age, id: Math.random().toString() },
      ];
    });
  };

  return (
    <div className="App">
      <AddUser onAddUser={handleAddUser} />
      <UsersList users={userList} />
    </div>
  );
}

export default App;
