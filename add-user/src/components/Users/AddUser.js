import { useState } from "react";
import Button from "../UI/Button";
import Card from "../UI/Card";
import classes from "./AdUser.module.css";
import Error from "../UI/Error";

const AddUser = (props) => {
  const [userName, setUserName] = useState("");
  const [age, setAge] = useState("");
  const [error, setError] = useState("");

  const addUserHandler = (event) => {
    event.preventDefault();
    if (userName.trim().length > 0 && age.trim().length > 0) {
      if (+age.trim() > 0) {
        props.onAddUser(userName, age);
        setUserName("");
        setAge("");
        setError("");
      } else
        setError({
          title: "Invalid age entry",
          message: "The person needs to be older than 0 years old",
        });
    } else
      setError({
        title: "Missing information",
        message: "Username and password are required",
      });
  };

  const userNameChangeHandler = (e) => {
    setUserName(e.target.value);
    setError("");
  };

  const ageChangeHandler = (e) => {
    setAge(e.target.value);
    setError("");
  };

  const errorHandler = () => {
    setError(null);
  };
  return (
    <div>
      {error && (
        <Error
          title={error.title}
          message={error.message}
          onClick={errorHandler}
        />
      )}
      <Card className={classes.input}>
        <form onSubmit={addUserHandler}>
          <label htmlFor="username">Username:</label>
          <input
            id="username"
            type="text"
            onChange={userNameChangeHandler}
            value={userName}
          />
          <label htmlFor="age">Age:</label>
          <input
            id="age"
            type="number"
            onChange={ageChangeHandler}
            value={age}
          />
          <Button type="submit">Add user</Button>
        </form>
      </Card>
    </div>
  );
};

export default AddUser;
